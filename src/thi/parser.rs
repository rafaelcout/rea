use crate::thi::lexer::ReaLexer;

pub struct Name(String);

pub enum Value {
    String(String)
}

pub enum Assignment {
    Direct(Name, Value)
}

pub enum Statement {
    Assignment(Assignment)
}

pub trait Parser<'chars> {
    fn new(lexer: ReaLexer<'chars>) -> Self;
    fn next(&mut self) -> Option<Statement>;
    fn ok(&mut self) -> bool;
}

pub struct ReaParser<'chars> {
    lexer: ReaLexer<'chars>
}

impl<'chars> Parser<'chars> for ReaParser<'chars> {
    fn new(lexer: ReaLexer<'chars>) -> Self {
        ReaParser {
            lexer
        }
    }

    fn ok(&mut self) -> bool {
        return false;
    }

    fn next(&mut self) -> Option<Statement> {
        None
    }

}
