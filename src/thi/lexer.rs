use std::str::Chars;
use std::iter::Peekable;

/// Rea code sources
pub enum Source {
    Code(&'static str)
}

/// Rea tokens
pub enum Token {
    Int,
    Float,
    String(String),
    Name(String),
    Assign,
    ParenthesesOpen,
    ParenthesesClose
}

/// Rea lexer trait
pub trait Lexer {
    /// New and usable lexer
    fn new(source: Source) -> Self;

    /// Is everything ok? true when usable
    fn ok(&mut self) -> bool;

    /// Next token
    fn next(&mut self) -> Option<Token>;
}

/// Rea lexer
pub struct ReaLexer<'chars> {
    chars: Option<Peekable<Chars<'chars>>>
}

impl<'chars> Lexer for ReaLexer<'chars> {
    /// New and usable lexer
    fn new(source: Source) -> Self {
        let chars : &str = match source {
            Source::Code(code) => { code }
        };

        ReaLexer {
            chars: Some(chars.chars().peekable())
        }
    }

    /// Is everything ok? true when usable
    fn ok(&mut self) -> bool {
        if self.chars.is_some() {
            return self.chars.as_mut().unwrap().peek().is_some();
        }

        false
    }

    /// Next token
    fn next(&mut self) -> Option<Token> {
        if self.chars.is_none() {
            return None;
        }

        let src = self.chars.as_mut().unwrap();

        let mut ch = match src.next() {
            Some(c) => c,
            None => return None    
        };

        // name
        if ch.is_ascii_alphabetic() {
            let mut name = String::new();

            loop {
                name.push(ch);
                ch = *src.peek().unwrap_or(&'0');

                if !ch.is_ascii_alphanumeric() {
                    break;
                }

                if ch == '0' {
                    break;
                }

                src.next();
            }

            print!(" name {}", name);
            return Some(Token::Name(name));
        }

        // =
        if ch == '=' {
            print!(" =");
            return Some(Token::Assign);
        }

        // 'string
        if ch == '\'' || ch == '\"' {
            let delim = ch;
            let mut string = String::new();

            loop {
                ch = src.next().unwrap_or('0');

                if ch == delim {
                    break;
                }

                if ch == '0' {
                    return None;
                }

                string.push(ch);
            }

            print!(" string {}", string);
            return Some(Token::String(string));
        }

        None
    }
}