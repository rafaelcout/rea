extern crate rea;

use rea::thi::lexer::{ReaLexer, Source, Lexer};
use rea::thi::parser::Parser;

#[test]
fn test() {
    let mut lex = ReaLexer::new(Source::Code(
        "name = 'Rea'"
    ));

    assert_eq!(lex.ok(), true);

    loop {
        if lex.next().is_none() {
            break;
        }
    }

    assert_eq!(lex.ok(), false);
}

#[test]
fn parser() {

}